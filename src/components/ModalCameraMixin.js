export default {
  data () {
    return {
      isShow: this.show,
      streamStarted: false,
      showScreenShot: false,
      pause: false,
      constraints: {
        video: {
          width: {
            min: 1280,
            ideal: 1920,
            max: 2560
          },
          height: {
            min: 720,
            ideal: 1080,
            max: 1440
          }
        }
      }
    }
  },
  watch: {
    show (val) {
      this.isShow = val
    },
    isShow (val) {
      this.$emit('update:show', val)
    }
  },
  mounted () {
    if (this.streamStarted) {
      this.$refs.video.play()
      return
    }
    if ('mediaDevices' in navigator && navigator.mediaDevices.getUserMedia) {
      const updatedConstraints = {
        ...this.constraints
      }
      this.startStream(updatedConstraints)
    }
  },
  methods: {
    onClickBtnTakePhoto () {
      const { canvas, video, screenshot } = this.$refs
      canvas.width = video.videoWidth
      canvas.height = video.videoHeight
      canvas.getContext('2d').drawImage(video, 0, 0)
      screenshot.src = canvas.toDataURL('image/webp')
      this.showScreenShot = true
      this.stopVideo()
      this.pause = true
    },
    onClickBtnPausePlay () {
      const { video, screenshot } = this.$refs
      screenshot.src = null
      this.showScreenShot = false
      if (!video.srcObject) {
        this.pause = false
        this.startStream({
          ...this.constraints
        })
        return
      }
      this.pause = !this.pause
      this.pause ? video.pause() : video.play()
    },
    stopVideo () {
      const video = this.$refs.video
      if (!this.streamStarted || !video.srcObject) {
        return
      }
      video.srcObject.getTracks()[0].stop()
      video.srcObject = null
    },
    onClickBtnClose () {
      this.stopVideo()
      this.isShow = false
    },
    async startStream (constraints) {
      const stream = await navigator.mediaDevices.getUserMedia(constraints)
      this.handleStream(stream)
    },
    handleStream (stream) {
      this.streamStarted = true
      this.$refs.video.srcObject = stream
    }
  }
}
