import recordrtc from 'recordrtc/RecordRTC.js'
import 'webrtc-adapter/out/adapter.js'

export default {
  methods: {
    requestAcces () {
      navigator.mediaDevices.getUserMedia({
        audio: true, 
        video: false
      }).then(this.onSuccessRequest)
    },
    onSuccessRequest (stream) {
      this.$refs.video.srcObject = stream
      this.$emit('succes-request')
    }
  }
}
