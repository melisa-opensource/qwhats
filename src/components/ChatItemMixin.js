export default {
  computed: {
    getLastMessageContent () {
      const typing = this.typing
      const content = this.lastMessage.content
      return typing ? this.$t('messenger.typing') : content
    },
    getAvatarImage () {
      return this.avatar
    },
    getLastMessageTime () {
      if (!this.lastMessage.createdAt) {
        return ''
      }
      const dateFormat = this.$moment(this.lastMessage.createdAt)
      return dateFormat.fromNow()
    }
  },
  methods: {
    onClickItem (record) {
      this.$emit('click', record)
    }
  }
}
