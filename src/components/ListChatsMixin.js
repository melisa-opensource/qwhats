export default {
  mounted () {
    this.$store.dispatch('chats/paging')
  },
  computed: {
    getListChats () {
      return this.$store.getters['chats/records']
    }
  },
  methods: {
    onClickItem (record) {
      this.$router.push({
        name: 'messenger.chat',
        params: {
          id: record.id
        }
      })
    }
  }
}
