export const records = (state) => (id) => {
  return state.records[id] || null
}
export const url = (state) => {
  return state.url
}
export const pagination = (state) => {
  return state.pagination
}
