export default {
  data () {
    return {
      messages: []
    }
  },
  mounted () {
    this.loadMessages(this.id)
  },
  watch: {
    'id' (id) {
      this.loadMessages(id)
    }
  },
  methods: {
    isMessageImage (message) {
      return message.type === 'image/png' || message.type === 'image/jpg'
    },
    isMessageText (message) {
      return message.type === 'text/plain' || message.type === 'text/markdown'
    },
    getFormatStamp (datetime) {
      return this.$moment(datetime).format('HH:mm a')
    },
    loadMessages (id) {
      this.$store.dispatch('messages/paging', {
        id: id
      }).then(this.onSuccessLoadMessages)
    },
    onSuccessLoadMessages (result) {
      this.messages = result
      this.$emit('loaded', result)
    }
  }
}
