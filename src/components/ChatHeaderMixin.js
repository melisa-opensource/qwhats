export default {
  data () {
    return {
      imageProfile: 'https://cdn.iconscout.com/icon/free/png-256/avatar-373-456325.png',
      search: ''
    }
  },
  methods: {
    onClickBtnBack () {
      return this.$router.push({
        name: 'messenger.view'
      })
    }
  }
}
