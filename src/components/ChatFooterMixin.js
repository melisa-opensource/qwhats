export default {
  data () {
    return {
      message: '',
      showAudioRecord: false
    }
  },
  methods: {
    onKeyupMessage (e) {
      if (e.keyCode !== 13) {
        return
      }
      this.$emit('add-message', this.message)
      this.message = ''
    },
    supportCamera () {
      if ('mediaDevices' in navigator && 'getUserMedia' in navigator.mediaDevices) {
        return true
      }
      return false
    },
    onClickBtnCamera () {
      this.$emit('show-modal-camera', true)
    },
    onClickBtnMicrophone () {
      this.showAudioRecord = true
    }
  }
}
