export const setRecords = (state, payload) => {
  state.records[payload.id] = payload.records
}
export const addMessage = (state, payload) => {
  const records = state.records[payload.id]
  records.push(payload.message)
  state.records[payload.id] = records
}
export const setPagination = (state, payload) => {
  state.pagination = payload
}
