
const routes = [
  {
    path: '/',
    component: () => import('layouts/View.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/Index.vue')
      },
      {
        path: ':id',
        name: 'messenger.chat',
        component: () => import('pages/Chat.vue')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
