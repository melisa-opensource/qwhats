export default {
  messenger: {
    typing: 'Escribiendo',
    view: {
      title: 'Messenger',
      chats: 'Chats',
      posts: 'Publicaciones'
    },
    chat: {
      typeMessage: 'Escribe el mensaje'
    },
    camera: {
      takePhoto: 'Tomar foto',
      pause: 'Pausar',
      play: 'Reproducir'
    }
  }
}
