import faker from 'faker/locale/es_MX'
import showdown from 'showdown'
import FakerMarkdown from 'src/helpers/FakerMarkdown'

export function paging (state, payload) {
  return new Promise((resolve, reject) => {
    let data = []
    faker.markdown = new FakerMarkdown(faker)
    const converter = new showdown.Converter()
    converter.setOption('strikethrough', true)
    for (let i = 0; i < 30; i++) {
      const typing = faker.random.boolean()
      const withMarkdown = faker.random.boolean()
      let content = faker.lorem.paragraph()
      if (withMarkdown) {
        content = faker.markdown.paragraphEmphasis()
        content = converter.makeHtml(content)
      }
      data.push({
        id: faker.random.uuid(),
        name: [
          faker.name.firstName(),
          faker.name.lastName()
        ].join(' '),
        lastMessage: {
          content: content,
          createdAt: faker.date.past()
        },
        typing: typing,
        avatar: [
          faker.image.imageUrl(null, null, 'cats')
        ].join('')
      })
    }
    setTimeout(() => {
      state.commit('setRecords', data)
      resolve()
    }, 600)
  })
}
