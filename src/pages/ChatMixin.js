import { scroll } from 'quasar'
const { setScrollPosition } = scroll

export default {
  data () {
    return {
      identity: null,
      identity_id: null,
      showModalCamera: false
    }
  },
  watch: {
    '$route' (to, from) {
      this.identity_id = to.params.id
    }
  },
  created () {
    this.identity_id = this.$route.params.id
  },
  methods: {
    onSuccessAddMessage (result) {
      this.onLoaded(100)
    },
    onErrorAddMessage (result) {
      debugger
      console.log(result)
    },
    onAddMessage (message) {
      this.$store.dispatch('messages/addMessage', {
        id: this.identity_id,
        message: message
      }).then(this.onSuccessAddMessage)
        .catch(this.onErrorAddMessage)
    },
    onLoaded () {
      this.scrolling(100)
    },
    scrolling (timeout, element) {
      const body = this.$refs.body.$el
      if (element) {
        return
      }
      setTimeout(() => {
        setScrollPosition(body, body.scrollHeight, 1000)
      }, timeout)
    },
    onShowModalCamera () {
      this.showModalCamera = true
    }
  }
}
