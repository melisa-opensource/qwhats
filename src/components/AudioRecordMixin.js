export default {
  data () {
    return {
      totalTime: 0,
      timer: null,
      player: null
    }
  },
  computed: {
    minutes () {
      const minutes = Math.floor(this.totalTime / 60)
      return this.padTime(minutes)
    },
    seconds () {
      const seconds = this.totalTime - (this.minutes * 60)
      return this.padTime(seconds)
    }
  },
  mounted () {
    this.startTimer()
  },
  methods: {
    startTimer () {
      this.timer = setInterval(() => this.countUp(), 1000)
    },
    padTime (time) {
      return (time < 10 ? '0' : '') + time
    },
    stopTimer () {
      clearInterval(this.timer)
      this.timer = null
      this.resetButton = true
    },
    countUp () {
      this.totalTime++
    },
    webrtcInitPlayer () {
      this.$refs.player.requestAcces()
    },
    videoJsRecordOnDeviceReady (player) {
      setTimeout(() => {
        player.requestFullscreen()
        setTimeout(() => {
          player.record().start()
        }, 1000)
      }, 1000)
    },
    onReadyVideoJsRecord (player) {
      this.player = player
      setTimeout(() => {
        let play = document.getElementsByClassName('vjs-icon-audio-perm')[0]
        if (play) {
          play.click()
        }
      }, 1000)
    }
  }
}
