export const setRecords = (state, payload) => {
  state.records = payload
}
export const setPagination = (state, payload) => {
  state.pagination = payload
}
