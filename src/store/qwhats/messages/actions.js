import fake from './fake'

export function addMessage (state, payload) {
  return new Promise((resolve, reject) => {
    const message = {
      type: 'text/plain',
      sent: true,
      status: 'sending',
      messages: [
        payload.message
      ]
    }
    state.commit('addMessage', {
      id: payload.id,
      message: message
    })
    resolve(message)
  })
}

export function paging (state, payload) {
  return new Promise((resolve, reject) => {
    const records = state.getters.records(payload.id)
    if (!records) {
      let records = fake.generate(3, payload)
      state.commit('setRecords', {
        id: payload.id,
        records: records
      })
      resolve(records)
      return
    }
    resolve(records)
  })
}
