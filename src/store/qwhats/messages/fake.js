import faker from 'faker/locale/es_MX'
import showdown from 'showdown'
import FakerMarkdown from 'src/helpers/FakerMarkdown'

const typeMessages = [
  'text/plain',
  'text/markdown'
  // 'image/png',
  // 'image/jpg',
  // 'video/webm',
  // 'audio/webm'
]
const status = [
  'sending',
  'sent',
  'received',
  'read'
]

faker.markdown = new FakerMarkdown(faker)
const converter = new showdown.Converter()
converter.setOption('strikethrough', true)

export default {
  generate (count, payload) {
    let records = []
    for (let i = 0; i < count; i++) {
      records.push(this.fake(payload))
    }
    return records
  },
  fake (payload) {
    const type = this.type()
    return {
      id: faker.random.uuid(),
      identity_id: payload.id,
      display: faker.name.title(),
      type: type,
      messages: this.messages(type),
      avatar: faker.image.imageUrl(null, null, 'cats'),
      sent: faker.random.boolean(),
      status: this.status(),
      createdAt: faker.date.past()
    }
  },
  status () {
    const position = faker.random.number(status.length - 1)
    return status[position]
  },
  type () {
    const position = faker.random.number(typeMessages.length - 1)
    return typeMessages[position]
  },
  messages (type) {
    let message
    switch (type) {
      case 'text/markdown':
        let content = faker.markdown.paragraphEmphasis()
        content = converter.makeHtml(content)
        message = [
          content
        ]
        break
      default:
        message = [
          faker.lorem.paragraph()
        ]
    }
    return message
  }
}
