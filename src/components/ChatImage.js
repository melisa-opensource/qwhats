import Vue from 'vue'
import { QChatMessage } from 'quasar'

export default Vue.extend({
  extends: QChatMessage,
  render (h) {
    console.log(this)
    return h('div', {
      staticClass: 'q-message',
      class: this.classes
    }, [
      this.label
        ? h('div', {
          staticClass: 'q-message-label text-center',
          domProps: { innerHTML: this.label }
        })
        : null,

      h('div', {
        staticClass: 'q-message-container hola row items-end no-wrap'
      }, [
        this.$scopedSlots.avatar !== void 0
          ? this.$scopedSlots.avatar()
          : (
            this.avatar !== void 0
              ? h('img', {
                staticClass: 'q-message-avatar col-auto',
                attrs: { src: this.avatar }
              })
              : null
          ),

        h('div', { class: this.sizeClass }, [
          this.name !== void 0
            ? h('div', {
              staticClass: 'q-message-name',
              domProps: { innerHTML: this.name }
            })
            : null,

          this.text !== void 0 ? this.__getText(h) : null,
          this.$scopedSlots.avatar !== void 0 ? this.$scopedSlots.avatar() : null
        ]),
        h('div', {
          class: 'image'
        }, [
          this.$slots.image !== void 0 ? this.$slots.image() : null
        ])
      ])
    ])
  }
})
