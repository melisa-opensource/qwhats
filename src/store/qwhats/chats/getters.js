export const records = (state) => {
  return state.records
}
export const url = (state) => {
  return state.url
}
export const pagination = (state) => {
  return state.pagination
}
