// https://github.com/collab-project/videojs-record/wiki/Webpack
import videojs from 'video.js'
import recordrtc from 'recordrtc/RecordRTC.js'
import 'webrtc-adapter/out/adapter.js'
import WaveSurfer from 'wavesurfer.js'
import MicrophonePlugin from 'wavesurfer.js/dist/plugin/wavesurfer.microphone.js'
WaveSurfer.microphone = MicrophonePlugin
import 'videojs-wavesurfer/dist/videojs.wavesurfer.js'
import 'videojs-record/dist/videojs.record.js'

import 'video.js/dist/video-js.min.css'
import 'videojs-record/dist/css/videojs.record.min.css'
import 'videojs-wavesurfer/dist/css/videojs.wavesurfer.min.css'

export default {
  data () {
    return {
      player: null,
      devices: [],
      options: {
        autoplay: true,
        controls: true,
        autoplay: this.autoPlay,
        fluid: false,
        loop: false,
        width: 400,
        height: 300,
        controlBar: {
          volumePanel: false,
          fullscreenToggle: true,
        },
        plugins: {
          wavesurfer: {
            src: 'live',
            waveColor: '#fff',
            progressColor: 'white',
            debug: true,
            cursorWidth: 1,
            msDisplayMax: 20,
            hideScrollbar: true
          },
          // configure videojs-record plugin
          record: {
            maxLength: 999,
            audio: true,
            video: false,
            debug: true
          }
        }
      }  
    }
  },
  beforeDestroy() {
    !this.player || this.player.dispose()
  },
  mounted () {
    this.createAudioRecord()
  },
  methods: {
    changeVideoInput (label, deviceId) {
      try {
        // change video input device
        this.player.record().setVideoInput(deviceId)
        console.log(`Changed video input to ${label} (deviceId: ${deviceId}`)
      } catch (error) {
        console.warn(error)
      }
      console.log('change video input success')
    },
    enumerateDevices () {
      this.player.record().enumerateDevices()
    },
    onEnumerateDevices () {
      this.devices = []
      let devices = this.player.record().devices
      console.log('total devices:', devices.length);
      devices.forEach((device) => {
        this.devices.push({
          id: device.deviceId,
          kind: device.kind,
          label: device.label || 'unknown'
        })
      })
    },
    onDeviceReady () {
      console.log('device is ready!')
      this.$emit('device-ready', this.player)
    },
    onStartRecord () {
      console.log('started recording!')
      this.$emit('start-record')
    },
    onFinishRecord () {
      // the blob object contains the recorded data that
      // can be downloaded by the user, stored on server etc.
      console.log('finished recording: ', this.player.recordedData)
      this.$emit('finish-record', this.player.recordedData)
    },
    onError (element, error) {
      console.warn(error)
      this.$emit('error', error)
    },
    onDeviceError () {
      console.error('device error:', this.player.deviceErrorCode)
      this.$emit('device-error', this.player.deviceErrorCode)
    },
    createAudioRecord () {
      this.player = videojs(this.$refs.audio, this.options, () => {
        // print version information at startup
        let msg = 'Using video.js ' + videojs.VERSION +
        ' with videojs-record ' + videojs.getPluginVersion('record') +
        ', videojs-wavesurfer ' + videojs.getPluginVersion('wavesurfer') +
        ', wavesurfer.js ' + WaveSurfer.VERSION + ' and recordrtc ' +
          recordrtc.version
        videojs.log(msg)
      })
      // enumerated devices
      this.player.on('enumerateReady', this.onEnumerateDevices.bind(this))

      // device is ready
      this.player.on('deviceReady', this.onDeviceReady.bind(this))

      // user clicked the record button and started recording
      this.player.on('startRecord', this.onStartRecord.bind(this))

      // user completed recording and stream is available
      this.player.on('finishRecord', this.onFinishRecord.bind(this))

      // error handling
      this.player.on('error', this.onError.bind(this))
      this.player.on('deviceError', this.onDeviceError.bind(this))
      this.$emit('init', this.player)
      this.player.on('ready', () => {
        console.log('ready')
        this.$emit('ready', this.player)
      })
    }
  }
}
