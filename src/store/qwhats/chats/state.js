export default {
  records: [],
  url: '/messenger/api/v1/messages',
  pagination: {
    page: 1,
    rowsNumber: 0,
    rowsPerPage: 25
  }
}
